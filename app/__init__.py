from flask import Flask
from flask_session import Session
from dotenv import load_dotenv
import os
import json

load_dotenv()

app = Flask(__name__)

app.config["SESSION_PERMANENT"] =os.getenv("SESSION_PERMANENET")
app.config["SESSION_TYPE"] =os.getenv("SESSION_TYPE")
Session(app)

## Dummy user and product data
users_json=os.getenv("USERS")
users=json.loads(users_json)

products = {
    1: {"name": "Laptop", "price": 1000},
    2: {"name": "Mouse", "price": 50},
    3: {"name": "Keyboard", "price": 100}
}

from app import routes

